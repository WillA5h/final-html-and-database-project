//The function that the HTML button activates
function validate(form)
{
    var ok=1
    var msg=""
    //For loop for every element in the form that the user had to enter
    for (var i = 0; i < form.length; i++)
    {
        //Checks if there is nothing in the textbox
        if (form.elements[i].value.trim() == "")
        {
            msg += "Please enter a movie title"
            ok = 0
        }
    }

    //If there are empty text boxes
    if (ok == 0)
    {
        alert(msg)
        return false
    }
    //If all boxes are filled
    else
    {
        return true
    }
}