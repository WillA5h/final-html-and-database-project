//The function that the HTML button activates
function validate(form)
{
    var ok=1
    var msg="Please fix the following issues with your form:"
    //For loop for every element in the form that the user had to enter
    for (var i = 0; i < form.length; i++)
    {
        //Checks if there is nothing in the textbox
        if (form.elements[i].value.trim() == "")
        {
            ok = 0;
            if (i == 0)
            {
                msg += "\nPlease enter a movie title"
            }
            if (i == 1)
            {
                msg += "\nPlease enter a movie price"
            }
            if (i == 2)
            {
                msg += "\nPlease enter an actor in the movie"
            }
            if (i == 3)
            {
                msg += "\nPlease enter a movie genre"
            }
            if (i == 4)
            {
                msg += "\nPlease enter a movie release date"
            }
        }
    }

    //If there are empty text boxes
    if (ok == 0)
    {
        alert(msg)
        return false
    }
    //If all boxes are filled
    else
    {
        return true
    }
}