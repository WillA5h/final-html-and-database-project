//The function enacts when there is scrolling on the page
window.onscroll = function() {scrollFN()}

//The scroll function
function scrollFN(){
    //Only sets the scroll to the top button when 200px down the page
    if (document.documentElement.scrollTop > 200)
    {
        document.getElementById("topbut").style.display = "block";
    }
    else
    {
        document.getElementById("topbut").style.display = "none";
    }
}

//Sets the scroll to the top
function topFN()
{
    document.documentElement.scrollTop = 0;
}