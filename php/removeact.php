<html>
  <head>
    <title>Film Database</title>
    <link rel="icon" href="https://www.pngrepo.com/png/283739/170/film-reel-movie.png">
    <style type="text/css">
    /* CSS code  */
      body{
        background-color: #95d4d6;
      } 
      h2{
        font-size: 35px;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        color: #17252A;
        text-align: left;
      }
      table{
        width: 100%;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        padding: 8px;
        text-align: left;
        border-collapse: collapse;
      }
      th{
        font-size: 25px;
      }
      td{
        font-size: 17px;
        height: 25px;
        vertical-align: center;
      }
      tr:nth-child(even){
        background-color: #7dbcc2
      }
      .row:after {
        content: "";
        display: table;
        clear: both;  
      }
      .columnbutton{
        float: left;
        width: 50%;
        padding: 0px;
      }
      button{
        border: none;
        background-color: #17252A;
        color: #3AAFA9;
        text-align: center;
        font-size: 20px;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        height: 45px;
        width: 100%;
        -webkit-transition-duration: 0.3s; 
        transition-duration: 0.3s;
      }
      button:hover{
        background-color: #3AAFA9; 
        color: #17252A;
        border: 4px solid #17252A;
      }
      h3{
        font-size: 25px;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        color: #17252A;
        text-align: center;
      }
    </style>
  </head>
  <body>
    <h2>Movie removal Result:</h2>
    <br>
    <?php
    //Gets the value that was entered into the actor search bar
      $actsearch = $_GET['title'];
      //MySQL login information
      $db_host = 'mysql.cs.nott.ac.uk';
      $db_user = 'psywa1'; 
      $db_pass = 'NZXT12'; 
      $db_name = 'psywa1'; 

      //Makes a connection to the database with the previously provided details
      $conn = new mysqli($db_host, $db_user, $db_pass, $db_name);
      //If it doenst connect, this kills the php program
      if ($conn->connect_errno)  die("failed to connect to database\n</body>\n</html>"); 
      
      //SQL statment to get the name of the actor name entered from the database 
      $sql="SELECT actName FROM Actor WHERE actName='$actsearch'";
      //Uses the connection and prepares it into a statment
      $stmt = $conn->prepare($sql);
      //executes the prepared statment
      $stmt->execute();
      //Gives the results to the respective variables
      $stmt->bind_result($name);
      //While the statment values are being fetched
      while($stmt->fetch())
      {
        //puts the information into a new variable
          $namenew = $name;
      }
      //Checks if the actor was actually in the database
      if ($namenew == $actsearch) 
      {
        //SQL statment to remove that actor from the databse
        $sql1="DELETE FROM Actor WHERE actName='$actsearch'";
        $stmt1 = $conn->prepare($sql1);
        $stmt1->execute();
        ?>
        <h3>Actor Successfully removed</h3><br>
        <?php
        //The code below then shows all the remaining actors, similar to the viewact.php code
        $sql="SELECT actID,actName FROM Actor";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($ID, $Name);
        ?>
        <table>  
          <tr> <th>ID</th> <th>Name</th></tr>
          <?php
          while($stmt->fetch())
          {
              echo "<tr>";
              echo "<td>". htmlentities($ID) ."</td>";
              echo "<td>". htmlentities($Name) ."</td>";
              echo "</tr>";
        }  
        ?>
        </table>
        <?php
      }
      //if the actor wasnt in the database
      if ($namenew != $actsearch) 
      {
        ?>
        <h3>There is no movie with that title.</h3>
        <?php
      }
      ?>
  
  <!-- Buttons to go back -->
  <br><br>
  <div class='row'>
    <div class="columnbutton">
      <form method="get" action="../index.html">
        <button type="submit" >Home</button>
      </form>
    </div>
    <div class="columnbutton">
      <form method="get" action="../removeActor.html">
        <button type="submit" >Remove another actor</button>
      </form>
    </div>
  </div>

  </body>
</html>
