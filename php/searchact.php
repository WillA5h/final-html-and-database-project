<html>
  <head>
    <title>Film Database</title>
    <link rel="icon" href="https://www.pngrepo.com/png/283739/170/film-reel-movie.png">
    <style type="text/css">
    /* CSS code  */
      body{
        background-color: #95d4d6;
      } 
      h2{
        font-size: 35px;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        color: #17252A;
        text-align: left;
      }
      table{
        width: 100%;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        padding: 8px;
        text-align: left;
        border-collapse: collapse;
      }
      th{
        font-size: 25px;
      }
      td{
        font-size: 17px;
        height: 25px;
        vertical-align: center;
      }
      tr:nth-child(even){
        background-color: #7dbcc2
      }
      .row:after {
        content: "";
        display: table;
        clear: both;  
      }
      .columnbutton{
        float: left;
        width: 50%;
        padding: 0px;
      }
      button{
        border: none;
        background-color: #17252A;
        color: #3AAFA9;
        text-align: center;
        font-size: 20px;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        height: 45px;
        width: 100%;
        -webkit-transition-duration: 0.3s; 
        transition-duration: 0.3s;
      }
      button:hover{
        background-color: #3AAFA9; 
        color: #17252A;
        border: 4px solid #17252A;
      }
      h3{
        font-size: 25px;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        color: #17252A;
        text-align: center;
      }
    </style>
  </head>
  <body>
    <h2>Actor Search Result:</h2>
    <br>
    <?php
      //Gets the value that was entered into the actor search bar
      $actorsearch = $_GET['title'];
      //MySQL login information
      $db_host = 'mysql.cs.nott.ac.uk';
      $db_user = 'psywa1'; 
      $db_pass = 'NZXT12'; 
      $db_name = 'psywa1'; 

      //Makes a connection to the database with the previously provided details
      $conn = new mysqli($db_host, $db_user, $db_pass, $db_name);
      //If it doenst connect, this kills the php program
      if ($conn->connect_errno)  die("failed to connect to database\n</body>\n</html>"); 

      //SQL statment to get all of the data of the actor entered from the database 
      $sql="SELECT actID,actName FROM Actor WHERE actName='$actorsearch'";
      //Uses the connection and prepares it into a statment
      $stmt = $conn->prepare($sql);
      //executes the prepared statment
      $stmt->execute();
      //Gives the results to the respective variables
      $stmt->bind_result($ID, $Name);
      //While the statment values are being fetched
      while($stmt->fetch())
      {
        //Checks if the actor name was in the database to begin with
        if ($actorsearch == $Name) 
        {
        ?>
        <table>
            <tr> <th>ID</th> <th>Name</th></tr>
            <!-- Creates the table and adds each result to it -->
            <?php
              echo "<tr>";
              echo "<td>". htmlentities($ID) ."</td>";
              echo "<td>". htmlentities($Name) ."</td>";
              echo "</tr>";
            ?>
        </table>
        <?php
        }
      }
      //If the name isnt in the database
      if ($actorsearch != $Name)
      {
        ?>
        <h3>There is no actor with that name.</h3>
        <?php
      } 
    ?>
    
    <?php
        //If the actors name is in the database
        if($actorsearch == $Name)
        {
            ?>
            <br>
            <h2>Movie(s) actor is in:</h2>
            <br>

            <?php
                //similar to what was done above, but gets the id and the title of the movie the actor was in
                $sql1="SELECT mvID, mvTitle FROM Movie, Actor WHERE actName='$actorsearch' AND Movie.actID=Actor.actID";
                $stmt1 = $conn->prepare($sql1);
                $stmt1->execute();
                $stmt1->bind_result($mvID, $mvName);
            ?>
            <table>
            <tr> <th>ID</th> <th>Movie</th></tr>
            <!-- Puts the data in a table -->
            <?php
                while($stmt1->fetch()){
                    echo "<tr>";
                    echo "<td>". htmlentities($mvID) ."</td>";
                    echo "<td>". htmlentities($mvName) ."</td>";
                    echo "</tr>";
                }
            ?>
            </table>
            <?php
        }
        ?>
    
  <!-- Buttons to go back -->    
  <br><br>
  <div class='row'>
    <div class="columnbutton">
      <form method="get" action="../index.html">
        <button type="submit" >Home</button>
      </form>
    </div>
    <div class="columnbutton">
      <form method="get" action="../searchActor.html">
        <button type="submit" >Go Back</button>
      </form>
    </div>
  </div>

  </body>
</html>
