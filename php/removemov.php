<html>
  <head>
    <title>Film Database</title>
    <link rel="icon" href="https://www.pngrepo.com/png/283739/170/film-reel-movie.png">
    <style type="text/css">
    /* CSS code  */
      body{
        background-color: #95d4d6;
      } 
      h2{
        font-size: 35px;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        color: #17252A;
        text-align: left;
      }
      table{
        width: 100%;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        padding: 8px;
        text-align: left;
        border-collapse: collapse;
      }
      th{
        font-size: 25px;
      }
      td{
        font-size: 17px;
        height: 25px;
        vertical-align: center;
      }
      tr:nth-child(even){
        background-color: #7dbcc2
      }
      .row:after {
        content: "";
        display: table;
        clear: both;  
      }
      .columnbutton{
        float: left;
        width: 50%;
        padding: 0px;
      }
      button{
        border: none;
        background-color: #17252A;
        color: #3AAFA9;
        text-align: center;
        font-size: 20px;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        height: 45px;
        width: 100%;
        -webkit-transition-duration: 0.3s; 
        transition-duration: 0.3s;
      }
      button:hover{
        background-color: #3AAFA9; 
        color: #17252A;
        border: 4px solid #17252A;
      }
      h3{
        font-size: 25px;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        color: #17252A;
        text-align: center;
      }
    </style>
  </head>
  <body>
    <h2>Movie removal Result:</h2>
    <br>
    <?php
    //Gets the value that was entered into the title search bar
      $titlesearch = $_GET['title'];
      //MySQL login information
      $db_host = 'mysql.cs.nott.ac.uk';
      $db_user = 'psywa1'; 
      $db_pass = 'NZXT12'; 
      $db_name = 'psywa1'; 

      //Makes a connection to the database with the previously provided details
      $conn = new mysqli($db_host, $db_user, $db_pass, $db_name);
      //If it doenst connect, this kills the php program
      if ($conn->connect_errno)  die("failed to connect to database\n</body>\n</html>"); 
      
      //SQL statment to get the title of the movie title entered from the database 
      $sql="SELECT mvTitle FROM Movie WHERE mvTitle='$titlesearch'";
      //Uses the connection and prepares it into a statment
      $stmt = $conn->prepare($sql);
      //executes the prepared statment
      $stmt->execute();
      //Gives the results to the respective variables
      $stmt->bind_result($title);
      //While the statment values are being fetched
      while($stmt->fetch())
      {
        //puts the information into a new variable
          $titlenew = $title;
      }
      //Checks if the title was actually in the database
      if ($titlenew == $titlesearch) 
      {
        //SQL statment to remove that movie from the databse
        $sql1="DELETE FROM Movie WHERE mvTitle='$titlesearch'";
        $stmt1 = $conn->prepare($sql1);
        $stmt1->execute();
        ?>
        <h3>Movie Successfully removed</h3>
        <?php
        //The code below then shows all the remaining movies, similar to the viewmov.php code
        $sql2="SELECT mvID,mvTitle,mvPrice,mvYear,mvGenre FROM Movie";
        $stmt2 = $conn->prepare($sql2);
        $stmt2->execute();
        $stmt2->bind_result($IDs, $Titles, $Prices, $Years, $Genres );
        ?>
            <br>
            <table>  
            <tr> <th>ID</th> <th>Title</th> <th>Price</th> <th>Year</th> <th>Genre</th> </tr>
            <?php
        while($stmt2->fetch())
        {
            
            echo "<tr>";
            echo "<td>". htmlentities($IDs) ."</td>";
            echo "<td>". htmlentities($Titles) ."</td>";
            echo "<td>". htmlentities($Prices) ."</td>";
            echo "<td>". htmlentities($Years) ."</td>";
            echo "<td>". htmlentities($Genres) ."</td>";
            echo "</tr>";
            
        }
        ?>
        </table>
        <?php
      }
      //if the movie wasnt in the database
      if ($titlenew != $titlesearch) 
      {
        ?>
        <h3>There is no movie with that title.</h3>
        <?php
      }
      ?>
  
  <!-- Buttons to go back -->
  <br><br>
  <div class='row'>
    <div class="columnbutton">
      <form method="get" action="../index.html">
        <button type="submit" >Home</button>
      </form>
    </div>
    <div class="columnbutton">
      <form method="get" action="../removeMovie.html">
        <button type="submit" >Remove another movie</button>
      </form>
    </div>
  </div>

  </body>
</html>
