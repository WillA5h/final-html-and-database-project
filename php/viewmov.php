<html>
  <head>
    <title>Film Database</title>
    <link rel="icon" href="https://www.pngrepo.com/png/283739/170/film-reel-movie.png">
    <style type="text/css">
      /* CSS code  */
      body{
        background-color: #95d4d6;
      } 
      h2{
        font-size: 35px;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        color: #17252A;
        text-align: left;
      }
      table{
        width: 100%;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        padding: 8px;
        text-align: left;
        border-collapse: collapse;
      }
      th{
        font-size: 25px;
      }
      td{
        font-size: 17px;
        height: 25px;
        vertical-align: center;
        
      }
      tr:nth-child(even){
        background-color: #7dbcc2
      }
      button{
        border: none;
        background-color: #17252A;
        color: #3AAFA9;
        text-align: center;
        font-size: 20px;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        height: 45px;
        width: 25%;
        -webkit-transition-duration: 0.3s; 
        transition-duration: 0.3s;
      }
      button:hover{
        background-color: #3AAFA9; 
        color: #17252A;
        border: 4px solid #17252A;
      }
      .wrapper{
        text-align: center;
      }
      h3{
        font-size: 25px;
        font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        color: #17252A;
        text-align: center;
      }
    </style>
  </head>
  <body>
    <h2>Movie Search Result:</h2>
    <br>
    <?php
      //MySQL login information
      $db_host = 'mysql.cs.nott.ac.uk';
      $db_user = 'psywa1'; 
      $db_pass = 'NZXT12'; 
      $db_name = 'psywa1'; 

      //Makes a connection to the database with the previously provided details
      $conn = new mysqli($db_host, $db_user, $db_pass, $db_name);
      //If it doenst connect, this kills the php program
      if ($conn->connect_errno)  die("failed to connect to database\n</body>\n</html>"); 

      //SQL statment to get all of the data from the database
      $sql="SELECT mvID,mvTitle,mvPrice,mvYear,mvGenre FROM Movie";
      //Uses the connection and prepares it into a statment
      $stmt = $conn->prepare($sql);
      //executes the prepared statment
      $stmt->execute();
      //Gives the results to the respective variables
      $stmt->bind_result($ID, $Title, $Price, $Year, $Genre );
    ?>
    <!-- Creates the table and adds each result to it -->
    <table>  
        <tr> <th>ID</th> <th>Title</th> <th>Price</th> <th>Year</th> <th>Genre</th> </tr>
        <?php
        while($stmt->fetch())
        {
            echo "<tr>";
              echo "<td>". htmlentities($ID) ."</td>";
              echo "<td>". htmlentities($Title) ."</td>";
              echo "<td>". htmlentities($Price) ."</td>";
              echo "<td>". htmlentities($Year) ."</td>";
              echo "<td>". htmlentities($Genre) ."</td>";
              echo "</tr>";
      }  
    ?>
    </table>
    
    <br><br>
    <!-- Buttons to go back -->
  <div class="wrapper">
    <form method="get" action="../index.html">
      <button type="submit" >Home</button>
    </form>
  </div>
  </body>
</html>
